/* Використовуючи класи створити з об'єктів 
https://fakestoreapi.com/products об'єкти 
{ 
name : "string" 
img : "string" 
productPrice : "number" 
productDescription : "stering" 
} 
На виході створити карточки товарів з використанням бібліотеки bootstrap*/

fetch('https://fakestoreapi.com/products')
  .then(response => response.json())
  .then(data => {
    // класи Bootstrap
    const classCards = ['card', 'm-2'];
    
    // карточки товару
    data.forEach(product => {
      const cardDiv = document.createElement('div');
      cardDiv.classList.add('card');
      classCards.forEach(className => cardDiv.classList.add(className));
      
      // зображення
      const imgElem = document.createElement('img');
      imgElem.src = product.image;
      imgElem.classList.add('card-img-top','image');
      cardDiv.appendChild(imgElem);
      
      // назва
      const titleElem = document.createElement('h5');
      titleElem.classList.add('card-title');
      titleElem.textContent = product.title;
      cardDiv.appendChild(titleElem);

      // котегорія
      const categoryElem = document.createElement('p');
      categoryElem.classList.add('card-text');
      categoryElem.textContent = `Category: ${product.category}`;
      cardDiv.appendChild(categoryElem)

      // ціна
      const priceElem = document.createElement('p');
      priceElem.classList.add('card-text');
      priceElem.textContent = `Price: ${product.price}$`;
      cardDiv.appendChild(priceElem);
      
      // опис
      const descElem = document.createElement('p');
      descElem.classList.add('card-text');
      descElem.textContent = product.description;
      cardDiv.appendChild(descElem);

      // кнопка
      const btnElem = document.createElement('a');
      btnElem.classList.add('btn', 'btn-primary');
      btnElem.textContent = 'Add to cart';
      cardDiv.appendChild(btnElem);

      //рейтинг
      const rateElem = document.createElement('p');
      rateElem.classList.add('card-text');
      rateElem.textContent =`Rate: ${product.rating.rate}, Count: ${product.rating.count}`;
      cardDiv.appendChild(rateElem);

      document.getElementById('cards').appendChild(cardDiv);
    });
  })
  .catch(error => console.log(error));
