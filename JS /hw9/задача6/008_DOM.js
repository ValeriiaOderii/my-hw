function selectFirstChild() {
    const list = document.getElementById('list');
    const firstChild = list.firstElementChild;
    console.log(firstChild.textContent);
}

function selectLastChild() {
    const list = document.getElementById('list');
    const lastChild = list.lastElementChild;
    console.log(lastChild.textContent);
}

function selectNextNode() {
    const list = document.getElementById('list');
    const selectedNode = document.querySelector('.selected');
    if (selectedNode) {
        const nextNode = selectedNode.nextElementSibling;
        if (nextNode) {
            selectedNode.classList.remove('selected');
            nextNode.classList.add('selected');
            console.log(nextNode.textContent);
        }
    } else {
        const firstChild = list.firstElementChild;
        if (firstChild) {
            firstChild.classList.add('selected');
            console.log(firstChild.textContent);
        }
    }
}

function selectPrevNode() {
    const list = document.getElementById('list');
    const selectedNode = document.querySelector('.selected');
    if (selectedNode) {
        const prevNode = selectedNode.previousElementSibling;
        if (prevNode) {
            selectedNode.classList.remove('selected');
            prevNode.classList.add('selected');
            console.log(prevNode.textContent);
        }
    } else {
        const lastChild = list.lastElementChild;
        if (lastChild) {
            lastChild.classList.add('selected');
            console.log(lastChild.textContent);
        }
    }
}

function createNewChild() {
    const list = document.getElementById('list');
    const newChild = document.createElement('li');
    newChild.textContent = '...List Item';
    list.appendChild(newChild);
}

function removeLastChild() {
    const list = document.getElementById('list');
    const lastChild = list.lastElementChild;
    if (lastChild) {
        list.removeChild(lastChild);
    }
}

function createNewChildAtStart() {
    const list = document.getElementById('list');
    const newChild = document.createElement('li');
    newChild.textContent = 'List Item...';
    const firstChild = list.firstElementChild;
    if (firstChild) {
        list.insertBefore(newChild, firstChild);
    } else {
        list.appendChild(newChild);
    }
}