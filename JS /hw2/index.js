//ТРИКУТНИК
for (let u = 0; u < 15; u++) {
    for (let i = 0; i < 15 - u - 1; i++) {
        document.write("&nbsp;")
    }
    for (let a = 0; a < u; a++) {
        document.write("*")
    }
    document.write("<br/>")
}

//РОМБ
for (let i = 0; i < 10; i++) {
    for (let j = 0; j < 10 - i - 1; j++) {
        document.write("&nbsp;")
    }
    for (let k = 0; k <= i; k++) {
        document.write("*")
    }
    document.write("<br/>")
}

for (let i = 10 - 1; i > 0; i--) {
    for (let j = 0; j < 10 - i; j++) {
        document.write("&nbsp;")
    }
    for (let k = 0; k < i; k++) {
        document.write("*")
    }
    document.write("<br/>")
}

//ПОРОЖНІЙ ПРЯМОКУТНИК 
for (let u = 0; u < 15; u++) {
    for (let i = 0; i < 18; i++) {
        if (u === 0 || u === 15 - 1 || i === 0 || i === 18 - 1) {
            document.write("*")
        } else {
            document.write("&nbsp;&nbsp;")
        }
    }
    document.write("<br/>")
}

