/*
+ Створити клас Animal 
+ та розширюючі його класи Dog, Cat, Horse.
+ Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. 
+ Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
+ Dog, Cat, Horse перевизначають методи makeNoise, eat.
+ Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
+ Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). 
+ Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
+ У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. 
+ У циклі надсилайте їх на прийом до ветеринара.
*/

function Doc() {

}

Doc.prototype.treatAnimal = function (animal) {
    console.log(animal.food, animal.location)
}

Doc.prototype.props = 1


function Animal(food, location) {
    this.food = food;
    this.location = location;
}

Animal.prototype.makeNoise = function () {
    console.log(`${this.name} Така тварина спить`);
}

Animal.prototype.eat = function () {
    console.log(`${this.name} Така тварина нямає`);
}

Animal.prototype.sleep = function () {
    console.log(`Тваринка спить`);
}


//makeNoise, eat.
function Dog(name, food, location) {
    this.name = name;
    this.food = food;
    this.location = location;
}

Dog.quantity = 0;

Dog.prototype = new Animal();

Dog.prototype.makeNoise = function () {
    console.log(`${this.name} Така тварина спить 10 годин`);
};

Dog.prototype.eat = function () {
    console.log(`${this.name} Така тварина нямає корм`);
};

function Cat(name, food, location) {
    this.name = name;
    this.food = food;
    this.location = location;
};

Cat.quantity = 0;

Cat.prototype = new Animal();

Cat.prototype.makeNoise = function () {
    console.log(`${this.name} Така тварина спить 5 годин`);
};

Cat.prototype.eat = function () {
    console.log(`${this.name}Така тварина нямає вкусняшку`);
};

function Horse(name, food, location) {
    this.name = name;
    this.food = food;
    this.location = location;
}

Horse.quantity = 0;

Horse.prototype = new Animal();

Horse.prototype.makeNoise = function () {
    console.log(`${this.name} Така тварина спить 2 години`);
};

Horse.prototype.eat = function () {
    console.log(`${this.name}Така тварина нямає моркву`);
};
const arr = [];
//main
function main() {
    const animal = [new Dog("Рей", "кістку", "у дворі"), new Cat("Рижуля", "мишку", "у будинку"), new Horse("Бархат", "сіно", "на полі")];

    const doc = new Doc();

    animal.forEach((item) => {
        doc.treatAnimal(item)
    })
};


main();
