/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* Зробити кнопку = робочою тобто true через javascript
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/
document.addEventListener('DOMContentLoaded', function () {
    let displayValue = '';
    let storedValue = '';
    let selectedOperator = '';
    let memory = null;

    const display = document.querySelector('.display input');
    const keys = document.querySelector('.keys');
    const equalsButton = document.getElementById('eq');

    keys.addEventListener('click', function (event) {
        const clickedButton = event.target;

        if (clickedButton.tagName === 'INPUT') {
            const buttonValue = clickedButton.value;

            if (!isNaN(buttonValue) || buttonValue === '.') {
                displayValue += buttonValue;
                enableEqualsButton();
            } else if (buttonValue === 'C') {
                displayValue = '';
                disableEqualsButton();
            } else if (['*', '/', '+', '-'].includes(buttonValue)) {
                if (storedValue !== '') {
                    displayValue = calculateResult();
                    console.log(`Calculation: ${storedValue} ${selectedOperator} ${displayValue}`);
                    storedValue = displayValue;
                }
                storedValue = displayValue;
                selectedOperator = buttonValue;
                displayValue = '';
            } else if (buttonValue === 'm-') {
                memory = memory ? memory - parseFloat(displayValue) : parseFloat(displayValue);
                displayValue = 'm' + displayValue;
            } else if (buttonValue === 'm+') {
                memory = memory ? memory + parseFloat(displayValue) : parseFloat(displayValue);
                displayValue = 'm' + displayValue;
            } else if (buttonValue === 'mrc') {
                if (memory !== null) {
                    displayValue = memory.toString();
                    memory = null;
                } else {
                    displayValue = '';
                }
            } else if (buttonValue === '=') {
                if (storedValue !== '' && selectedOperator !== '') {
                    displayValue = calculateResult();
                    storedValue = '';
                    selectedOperator = '';
                    disableEqualsButton();
                }
            }

            display.value = displayValue;
        }
    });

    function calculateResult() {
        const num1 = parseFloat(storedValue);
        const num2 = parseFloat(displayValue);

        switch (selectedOperator) {
            case '+':
                return (num1 + num2).toString();
            case '-':
                return (num1 - num2).toString();
            case '*':
                return (num1 * num2).toString();
            case '/':
                return (num1 / num2).toString();
            default:
                return displayValue;
        }
    }

    function enableEqualsButton() {
        equalsButton.disabled = false;
    }

    function disableEqualsButton() {
        equalsButton.disabled = true;
    }
});
