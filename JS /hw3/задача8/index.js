//Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].
document.write("<hr/>");
document.write("Задача 8");
document.write("<hr/>");
let a = [1, 2, 3, 4, 5];
let d = a.splice(1, 2);
document.write("<p>Видалені: " + d);
document.write("<p>Залишились: " + a);
document.write("<hr/>");