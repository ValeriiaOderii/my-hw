/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), 
які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, 
її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png
*/

class Driver {
    constructor(name, drivingExperience) {
        this.name = name;
        this.drivingExperience = drivingExperience;
    };
};

class Engine {
    constructor(power, manufacturer) {
        this.power = power;
        this.manufacturer = manufacturer;
    };
};

class Car {
    constructor(carBrand, carClass, weight, driver, engine) {
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    };

    start() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Поїхали`);
    };

    stop() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Зупиняємося`);
    };

    turnRight() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Поворот праворуч`);
    };

    turnLeft() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Поворот ліворуч`);
    };

    toString() {
        return `Автомобіль марки: ${this.carBrand}, Клас: ${this.carClass}, Вага: ${this.weight} кг\n` +
            `Водій: ${this.driver.name}, Стаж водіння: ${this.driver.drivingExperience} років\n` +
            `Двигун: Потужність - ${this.engine.power} к.с., Виробник - ${this.engine.manufacturer}`;
    };
};

const driver = new Driver('Віктор Матійко', 6);
const engine = new Engine(270, 'Peugeot');
const car = new Car('Peugeot', 'Sedan', 1575, driver, engine);
car.start();
car.stop();
car.turnRight();
car.turnLeft();
console.log(car.toString());

class Lorry extends Car {
    constructor(carBrand, carClass, weight, driver, engine, cargoCapacity) {
        super(carBrand, carClass, weight, driver, engine);
        this.cargoCapacity = cargoCapacity;
    };
    start() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Поїхали`);
    };

    stop() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Зупиняємося`);
    };

    turnRight() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Поворот праворуч`);
    };

    turnLeft() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Поворот ліворуч`);
    };

    toString() {
        return `Автомобіль марки: ${this.carBrand}, Клас: ${this.carClass}, Вага: ${this.weight} кг\n` +
            `Водій: ${this.driver.name}, Стаж водіння: ${this.driver.drivingExperience} років\n` +
            `Двигун: Потужність - ${this.engine.power} к.с., Виробник - ${this.engine.manufacturer}\n` +
            `Вантажопідйомністm кузова: ${this.cargoCapacity} т`;
    };
};

const driver1 = new Driver('Микола Мазур', 10);
const engine1 = new Engine(420, 'MAN SE');
const lorry = new Lorry('Man', 'Truck', 7000, driver1, engine1, 4.31);
lorry.start();
lorry.stop();
lorry.turnRight();
lorry.turnLeft();
console.log(lorry.toString());

class SportCar extends Car {
    constructor(carBrand, carClass, weight, driver, engine, maxSpeed) {
        super(carBrand, carClass, weight, driver, engine);
        this.maxSpeed = maxSpeed;
    };
    start() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Поїхали`);
    };

    stop() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Зупиняємося`);
    };

    turnRight() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Поворот праворуч`);
    };

    turnLeft() {
        console.log(`Водій автомообіля марки ${this.carBrand}:Поворот ліворуч`);
    };

    toString() {
        return `Автомобіль марки: ${this.carBrand}, Клас: ${this.carClass}, Вага: ${this.weight} кг\n` +
            `Водій: ${this.driver.name}, Стаж водіння: ${this.driver.drivingExperience} років\n` +
            `Двигун: Потужність - ${this.engine.power} к.с., Виробник - ${this.engine.manufacturer}\n` +
            `Гранична швидкість: ${this.maxSpeed} км/год`;
    };
};

const driver2 = new Driver('Данил Романік', 2);
const engine2 = new Engine(1622, 'Koenigsegg');
const sportCar = new SportCar('Koenigsegg Jesko Absolut', 'Sports', 1420, driver2, engine2, 532);
sportCar.start();
sportCar.stop();
sportCar.turnRight();
sportCar.turnLeft();
console.log(sportCar.toString());